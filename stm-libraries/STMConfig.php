<?php
/**
 * Developer: Rey Philip Regis
 */

class STMConfig {
	// property that will hold the contents of the json file
	protected $contents = null;

	// constructor
	public function __construct() {
		$rawContents 	= file_get_contents($_SERVER['DOCUMENT_ROOT'].'/stm-libraries/config.json');
		$this->contents = json_decode($rawContents, true);
	}

	// method that gets all the contents of a json file in array form
	public function getContents() {
		return $this->contents;
	}

	// method that gets a specific content in the config json file
	// @param String $property
	public function getSpecificContent($property) {
		return $this->contents[$property];
	}
}

?>
