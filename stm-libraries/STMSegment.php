<?php
ini_set('display_errors', 'on');

/**
 * Developer: Rey Philip Regis
 */

require_once 'vendors/analytics-php-master/lib/Segment.php';
require_once 'STMConfig.php';

class STMSegment {

    // property that will hold the config object
    protected $config = null;

    // method that tracks events
    public function track($event, $Arrayproperties, $amember_id = null) {
        // initialize config object
        $config = new STMConfig();
        $config->getSpecificContent('segment_token');

        class_alias('Segment', 'Analytics');
        Analytics::init(
            $config->getSpecificContent('segment_token')
        );

        // if there was no amember_id passed then use the amember_id in the cookie
        if ($amember_id === '' || is_null($amember_id)) {
            $amember_id = $_COOKIE['amember_id'];
        } 

        Analytics::track(
            array(
                "userId"     => $amember_id,
                "event"      => $event,
                "properties" => $Arrayproperties
            )
        );
    }

    public function identify($amember_id = null, $traitsArray) {
        // initialize config object
        $config = new STMConfig();
        
        Segment::init(
            $config->getSpecificContent('segment_token')
        );

        // if there was no amember_id passed then use the amember_id in the cookie
        if ($amember_id === '' || is_null($amember_id)) {
            $amember_id = $_COOKIE['amember_id'];
        }

        Segment::identify(array(
            "userId" => $amember_id,
            "traits" => $traitsArray
        ));
    }

    public function alias($previousId, $userId) {
        // initialize config object
        $config = new STMConfig();
        $config->getSpecificContent('segment_token');

        class_alias('Segment', 'Analytics');
        Analytics::init(
            $config->getSpecificContent('segment_token')
        );
        
        Analytics::alias(array(
            "previousId" => $previousId,
            "userId"     => $userId
        ));
    }
}

?>
