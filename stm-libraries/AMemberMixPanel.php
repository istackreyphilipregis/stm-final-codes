<?php

/**
 * Developer: Rey Philip Regis
 */

require $_SERVER['DOCUMENT_ROOT'].'/stm-libraries/vendors/mixpanel-php/lib/Mixpanel.php';
require $_SERVER['DOCUMENT_ROOT'].'/stm-libraries/STMConfig.php';

class AMemberMixPanel {

	// property that will hold the mixpanel object
    protected $mp = null;

    // property that will hold the config object
    protected $config = null;

    // property that will hold the mixpanel token api
    protected $mixpanel_token_api = null;

    // constructor
    public function __construct() {
        // initialize config object
        $config   = new STMConfig();

        // store the mixpanel token api
        $this->mixpanel_token_api = $config->getSpecificContent('mixpanel_token');

        // initialize mixpanel object
        $this->mp = Mixpanel::getInstance($this->mixpanel_token_api);
    }

    // method that returns a mix panel object
    public function getMixPanelObject() {
    	return $this->mp;
    }

    // method that get's the mixpanel token api
    public function getMixPanelTokenAPI() {
        return $this->mixpanel_token_api;
    }
}

?>
