<?php
error_reporting(E_ALL);
ini_set('display_errors', 'on');

class VBulletinDB {
	protected $connection;
	protected $config;

	public function __construct($config) {
		$this->config = $config;
	}

	public function connect() {
		//Connect to MySQL Server
		$this->connection = mysql_connect(
								$this->config['MasterServer']['servername'], 
								$this->config['MasterServer']['username'], 
								$this->config['MasterServer']['password']
							);

		// //Select Database
		mysql_select_db($this->config['Database']['dbname']) or die(mysql_error());

		return $this->connection;
	}

	public function queryData($query) {
		$this->connection = $this->connect();
		$result = mysql_query($query, $this->connection) or die(mysql_error());

		$i      = 0;
		$data   = array();
		while ($row  = mysql_fetch_assoc($result)) {
		    $data[$i]= $row;
		    $i++;
		}

		return $data;
	}
}

?>
