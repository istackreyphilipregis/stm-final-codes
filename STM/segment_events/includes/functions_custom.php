<?php
/*======================================================================*\
|| #################################################################### ||
|| # vBulletin 4.2.3 Patch Level 2 - Licence Number VBC0955432
|| # ---------------------------------------------------------------- # ||
|| # Copyright ©2000-2016 vBulletin Solutions Inc. All Rights Reserved. ||
|| # This file may not be redistributed in whole or significant part. # ||
|| # ---------------- VBULLETIN IS NOT FREE SOFTWARE ---------------- # ||
|| # http://www.vbulletin.com | http://www.vbulletin.com/license.html # ||
|| #################################################################### ||
\*======================================================================*/

function getNumberOfDaysByDates($startDate, $endDate) {
	return ceil(abs($endDate - $startDate) / 86400);
}

function getForumInfo($id)
{
	global $vbulletin;

	$query = $vbulletin->db->query_read("Select * from forum Where forumid=$id");

	$i     = 0;
	$data  = array();
	while ($row = $vbulletin->db->fetch_array($query))
	{
		$data[$i] = $row;

		$i++;
	}

	return $data;
}

function getThreadInfo($id)
{
	global $vbulletin;

	$query = $vbulletin->db->query_read("Select * from thread Where threadid=$id");
	
	$i     = 0;
	$data  = array();
	while ($row = $vbulletin->db->fetch_array($query))
	{
		$data[$i] = $row;

		$i++;
	}

	return $data;
}

function getUserInfo($id)
{
	global $vbulletin;

	$query = $vbulletin->db->query_read("Select * from user Where userid=$id");
	
	$i     = 0;
	$data  = array();
	while ($row = $vbulletin->db->fetch_array($query))
	{
		$data[$i] = $row;

		$i++;
	}

	return $data;
}

function getUserInfoByAMemberID($id)
{
	global $vbulletin;

	$query = $vbulletin->db->query_read("Select * from user Where amemberid=$id");
	
	$i     = 0;
	$data  = array();
	while ($row = $vbulletin->db->fetch_array($query))
	{
		$data[$i] = $row;

		$i++;
	}

	return $data;
}

function getUserInfoByUsername($username)
{
	global $vbulletin;

	$query = $vbulletin->db->query_read("Select * from user Where username='".$username."'");
	
	$i     = 0;
	$data  = array();
	while ($row = $vbulletin->db->fetch_array($query))
	{
		$data[$i] = $row;

		$i++;
	}

	return $data;
}

function getPostInfo($id)
{
	global $vbulletin;

	$query = $vbulletin->db->query_read("Select * from post Where postid=$id");
	
	$i     = 0;
	$data  = array();
	while ($row = $vbulletin->db->fetch_array($query))
	{
		$data[$i] = $row;

		$i++;
	}

	return $data;
}

function getHierarchyByParentList($parentList)
{
	global $vbulletin;

	$query     = $vbulletin->db->query_read("Select * from forum Where forumid IN($parentList)");
	$i         = 0;
	$hierarchy = array();
	while ($row = $vbulletin->db->fetch_array($query))
	{
		$hierarchy[$i] = $row;

		$i++;
	}

	// get the forum category
	$forumCategory = $hierarchy[count($hierarchy) - 1]['title_clean'];

	// get the subforum
	$reverseHierarchy = array_reverse($hierarchy);
	$subforum = '';
	for($i=0; $i<count($reverseHierarchy); $i++) {
		$subforum .= $reverseHierarchy[$i]['title_clean'];

		if ($i !== (count($reverseHierarchy) -1 )) {
			$subforum .= " > ";
		}
	}

	return [
		'category' => $forumCategory,
		'subforum' => $subforum
	];
}

/*======================================================================*\
|| ####################################################################
|| # Downloaded: 08:29, Mon Sep 12th 2016
|| # CVS: $RCSfile$ - $Revision: 32878 $
|| ####################################################################
\*======================================================================*/
?>