<!DOCTYPE html>
<html>
<head>
    <title>STM Forum - The #1 Affiliate Marketing Forum</title>
    <script type="text/javascript" src="//cdn-3.convertexperiments.com/js/10004357-10005304.js"></script>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description"
          content="STM Forum is the #1 rated community for affiliate and performance internet marketers. See how we generated $XX,XXX,XXX of affiliate revenue in 2015 and join our private forums.">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"/>
    <meta property="og:title" content="STM Forum - The #1 Affiliate Marketing Forum"/>
    <meta property="og:type" content="website"/>
    <meta property="og:url" content="http://stmforum.com"/>
    <meta property="og:image" content="http://stmforum.com/assets/images/meta-img.jpg"/>
    <meta property="og:description" content="STM Forum is the #1 rated community for affiliate and performance internet marketers. See how we generated $XX,XXX,XXX of affiliate revenue in 2015 and join our private forums."/>

    <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-touch-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-touch-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-touch-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-touch-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-touch-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-touch-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-touch-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-touch-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-touch-icon-180x180.png">
    <link rel="icon" type="image/png" href="favicon/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="favicon/android-chrome-192x192.png" sizes="192x192">
    <link rel="icon" type="image/png" href="favicon/favicon-96x96.png" sizes="96x96">
    <link rel="icon" type="image/png" href="favicon/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="msapplication-TileImage" content="favicon/mstile-144x144.png">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" href="styles/lib-f6b003b677.css">

    <link rel="stylesheet" href="styles/app-aee4798bff.css">

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- gsap -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/1.18.2/TweenMax.min.js"></script>

    <!-- google fonts -->
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300|Raleway:400,700,300,800,900' rel='stylesheet'
          type='text/css'>



        <script type="text/javascript">
            window._vis_opt_queue = window._vis_opt_queue || [];
            window._vis_opt_queue.push(function() {_vis_opt_goal_conversion(200);});
        </script>

        <script type="text/javascript">
            setTimeout(function(){var a=document.createElement("script");
                var b=document.getElementsByTagName("script")[0];
                a.src=document.location.protocol+"//script.crazyegg.com/pages/scripts/0053/5893.js?"+Math.floor(new Date().getTime()/3600000);
                a.async=true;a.type="text/javascript";b.parentNode.insertBefore(a,b)}, 1);
        </script>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

            ga('create', 'UA-81636791-1', 'auto');
            ga('send', 'pageview');

        </script>

</head>
<body>

<img src="assets/images/nav-open.png" class="nav-open" alt="nav-icon"/>

<section id="nav-wrapper">
    <div class="nav-content">
        <img src="assets/images/nav-close.png" class="nav-close" alt="nav-icon"/>
        <ul class="primary-nav">
            <!--<li><a href="#">about</a></li>-->
            <!--<li><a href="#">blog</a></li>-->
            <!--<li><a href="#">meetups</a></li>-->
            <li><a href="http://stmforum.com/forum/forum.php" class="stm-login"><i class="fa fa-lock"></i>login</a></li>
            <!--<li><a href="#">signup</a></li>-->
        </ul>
        <span class="line"></span>
        <ul class="secondary-nav">
            <li><span>our partners</span></li>
            <li><a href="http://affiliateworldconferences.com/" target="_blank">awc</a></li>
            <li><a href="http://affiliatemasterychallenge.com/" target="_blank">6wamc</a></li>
            <li><a href="http://stmforum.com/meetups/" target="_blank">meetups</a></li>
            <li><a href="http://stmblog.ru/" target="_blank">stm russia</a></li>
            <li><a href="http://duiqian8.com/" target="_blank">duiqianba</a></li>
            <li><a href="https://www.funnelflux.com/get/" target="_blank">funnelflux</a></li>
            <li><a href="https://adplexity.com/" target="_blank">adplexity</a></li>
            <li><a class="prime" href="http://stmforum.com/preview" target="_blank">preview stm forum</a></li>
        </ul>
        <ul class="social-nav">
            <li><a href="https://www.facebook.com/stmforum" target="_blank"><i class="fa fa-facebook"></i></a></li>
            <li><a href="https://twitter.com/stmforum" target="_blank"><i class="fa fa-twitter"></i></a></li>
            <li><a href="https://www.instagram.com/stmforum/" target="_blank"><i class="fa fa-instagram"></i></a></li>
            <li><a href="https://www.youtube.com/channel/UCiIGcnuSoTBqiYYs0j4hEpg" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
        </ul>
    </div>
</section>

<div id="main-wrapper">
    <section class="header-top-list">
        <div class="top-nav container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <ul>
                        <li><span>our partners:</span></li>
                        <li><a href="http://affiliateworldconferences.com/" target="_blank">awc</a></li>
                        <li><a href="http://affiliatemasterychallenge.com/" target="_blank">6wamc</a></li>
                        <li><a href="http://stmforum.com/meetups/" target="_blank">meetups</a></li>
                        <li><a href="http://stmblog.ru/" target="_blank">stm russia</a></li>
                        <li><a href="http://duiqian8.com/" target="_blank">duiqianba</a></li>
                        <li><a href="https://www.funnelflux.com/get/" target="_blank">funnelflux</a></li>
                        <li><a href="https://adplexity.com/" target="_blank">adplexity</a></li>
                        <li><a class="prime" href="http://stmforum.com/preview" target="_blank">preview stm forum</a></li>
                    </ul>
                    <ul>
                        <li><a href="https://www.facebook.com/stmforum" target="_blank"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="https://twitter.com/stmforum" target="_blank"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="https://www.instagram.com/stmforum/" target="_blank"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="https://www.youtube.com/channel/UCiIGcnuSoTBqiYYs0j4hEpg" target="_blank"><i class="fa fa-youtube-play"></i></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </section>
    <header id="main-header">
        <video autoplay loop class="bgvid">
            <source src="/assets/images/video-bg.mp4" type="video/mp4">
            <source src="/assets/images/video-bg.webm" type="video/webm">
        </video>
        <div class="header-content">
            <section class="header-logo-nav">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <img src="assets/images/stm-logo-white.png" alt="STM Logo" class="header-logo"/>
                            <ul class="header-nav">
                                <!--<li><a href="#">about</a></li>-->
                                <!--<li><a href="#">blog</a></li>-->
                                <!--<li><a href="#">meetups</a></li>-->
                                <li><a href="http://stmforum.com/forum/forum.php" class="active stm-login"><i class="fa fa-lock"></i>login</a></li>
                                <!--<li><a href="http://stmforum.com/amember/signup" >sign up</a></li>-->
                            </ul>
                        </div>
                    </div>
                </div>
            </section>
            <section class="header-slogan">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12 col-lg-9 col-lg-offset-3">
                            <h1>The #1 Premium Affiliate Marketing Community</h1>
                        </div>
                        <div class="col-md-12 col-md-offset-0 col-lg-5 col-lg-offset-7">
                            <p>STM Forum is your gateway to affiliate marketing.</p>
                            <p class="m-b-lg">Whether you are experienced and need a networking hub, or want to launch your first campaign online, we have something for you.</p>
                            <a href="https://reystmdev.istackmanila.com/join/">join now</a>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </header>

    <section id="aboutus-content">
        <div class="container">
            <div class="row">
                <div class="about-us col-lg-4 col-xs-12 col-sm-12 col-md-12 ">
                    <h1 class="the-title">
                        ABOUT US<span class="red-dot">.</span>
                    </h1>
                    <p class="the-description">THE AUTHORITATIVE SOURCE ON ALL<br> THINGS AFFILIATE MARKETING,<br>
                        <span class="since">SINCE 2011</span></p>
                </div>
                <div class="stm-para col-xs-12 col-sm-12 col-md-12 col-lg-8">
                    <p class="m-b-md"><span>STM Forum</span> was launched late one night in <span>January 2011</span> with the goal of providing <span>case studies,</span> <span>guides, high quality information</span> on a variety of topics and to act as a core <span>networking hub for industry veterans.</span> </p>
                    <p>5 years later we are ​ <span>the authoritative forum on affiliate marketing</span> and our reach continues to expand. We now provide <span>higher education, host conferences and local networking events,</span> and are continuing to improve and expand what the forum offers every affiliate marketer.</p>
                    <div class="btn-wrapper">
                        <a class="btn-join" href="https://reystmdev.istackmanila.com/join/">JOIN STM NOW</a><a class="btn-preview" href="http://stmforum.com/preview/">PREVIEW SOME CONTENT</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="blog-content">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="blog-container">
                        <h1 class="the-title">Blog Post</h1>

                        <!--<p class="the-description">Lorem ipsum dolor sit amet, consectetur adipisi</p>-->

                        <!-- controller for blog post -->
                        <div class="blog-controller">
                            <span class="arrow-left blog-btn-prev"></span>
                            <span class="arrow-right blog-btn-next"></span>
                            <span class="divider"></span>
                            <!-- increment/decrement this every click-->
                            <span class="number-top">1</span>
                            <span class="number-bottom">8</span>
                        </div>
                        <!-- end of controller -->

                        <a href="http://stmforum.com/blog" class="blog-link-3" target="_blank">go to stm.blog</a>
                    </div>
                </div>
            </div>
        </div>

        <section class="blog-article-slider">
            <?php

            include('config.php');
            include('GhostBlogApi.php');

            $ghost = new GhostBlogApi($config['ghost']);
            $posts = $ghost->getPosts();
            //                        echo '<pre>';var_dump($posts);
            $i = 1;
            foreach ($posts['posts'] as $post):
                ?>
                <article class="blog-article blog-article-<?php echo $i; ?>"
                         data-ghost-blog-image="<?php echo $post['image']; ?>">
                    <h1><?php echo $post['title']; ?></h1>
                    <h6>By <?php echo $post['author']['name']; ?> •
                        <?php
                        $dateCreated = date_create($post['published_at']);
                        echo date_format($dateCreated, "F d, Y");
                        ?></h6>

                    <p><?php echo strip_tags($post['html']); ?></p>
                    <a href="<?php echo $config['ghost']['stm_blog_url'] . $post['url']; ?>">Read full blog <span
                            class="arrow-right"></span></a>
                    <a href="http://stmforum.com/blog" class="blog-link" target="_blank">go to stm.blog</a>
                </article>
                <?php $i++;endforeach; ?>
        </section>
        <div class="blog-slider-container">
            <!--<img src="assets/images/blog-slider.jpg">-->
            <a href="http://stmforum.com/blog" class="blog-link-2" target="_blank">go to stm.blog</a>
        </div>
    </section>

    <!--<section id="event-content">-->
    <!--<div class="container">-->
    <!--<div class="row">-->
    <!--<div class="col-lg-12">-->
    <!--<h1 class="the-title">Upcoming meetups</h1>-->

    <!--<p class="the-description">Lorem ipsum dolor sit amet, consectetur adipisi</p>-->
    <!--</div>-->
    <!--<div class="col-md-4">-->
    <!--<div class="event-container m-t-md">-->
    <!--<div class="filter-container">-->
    <!--<div class="event-description">-->
    <!--<h4>Event Title Event Title</h4>-->

    <!--<p class="short-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do-->
    <!--eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>-->

    <!--<p class="long-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do-->
    <!--eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,-->
    <!--quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.-->
    <!--Duis aute irure dolor in reprehenderit. Lorem ipsum dolor sit amet, consectetur-->
    <!--adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna-->
    <!--aliqua.</p>-->

    <!--</div>-->
    <!--<a href="#">Read Full Details</a>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--<div class="col-md-4">-->
    <!--<div class="event-container m-t-md">-->
    <!--<div class="filter-container">-->
    <!--<div class="event-description">-->
    <!--<h4>Event Title Event Title</h4>-->

    <!--<p class="short-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do-->
    <!--eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>-->

    <!--<p class="long-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do-->
    <!--eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,-->
    <!--quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.-->
    <!--Duis aute irure dolor in reprehenderit. Lorem ipsum dolor sit amet, consectetur-->
    <!--adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna-->
    <!--aliqua.</p>-->

    <!--</div>-->
    <!--<a href="#">Read Full Details</a>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--<div class="col-md-4">-->
    <!--<div class="event-container m-t-md">-->
    <!--<div class="filter-container">-->
    <!--<div class="event-description">-->
    <!--<h4>Event Title Event Title</h4>-->

    <!--<p class="short-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do-->
    <!--eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>-->

    <!--<p class="long-desc">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do-->
    <!--eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,-->
    <!--quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.-->
    <!--Duis aute irure dolor in reprehenderit. Lorem ipsum dolor sit amet, consectetur-->
    <!--adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna-->
    <!--aliqua.</p>-->

    <!--</div>-->
    <!--<a href="#">Read Full Details</a>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--</div>-->
    <!--</section>-->

    <section class="upcoming-event">

        <h1 class="the-title">Upcoming Local Networking Events</h1>

        <p class="the-description">We are currently finalizing our events line up for 2016. Stay tuned.</p>

        <img src="assets/images/stm-events.png" alt=""/>
    </section>

    <footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-9 col-lg-offset-3">
                    <h1>JOIN THE #1 RATED FORUM IN THE INDUSTRY.</h1>

                    <h1>GET STARTED NOW!</h1>
                </div>
                <div class="col-md-6 col-lg-4">
                    <h6>STM Forum is the edge every marketer needs to get ahead in this industry. Click that sleek
                        button to learn more about exactly what it is we offer. Do it.</h6>
                </div>
                <div class="col-md-6 col-lg-4">
                    <a href="https://reystmdev.istackmanila.com/join/" class="footer-btn-join">Join Now!</a>
                </div>
            </div>
        </div>

        <footer class="bottom-footer">
            <div class="container">
                <div class="row">
                    <div class="col-lg-2 col-sm-3">
                        <img src="assets/images/stm-logo-white.png" class="logo-footer" alt="logo footer"/>
                    </div>
                    <div class="col-lg-10 col-sm-9">
                        <ul>
                            <li>
                                <p>© 2016 STM Forum. All Rights Reserved. <a href="http://stmforum.com/privacy.html"
                                                                             class="footer-mail-link" target="_blank">
                                        Privacy Policy.</a></p>
                            </li>
                            <li>
                                <p>Questions? <a href="mailto:info@stmforum.com" class="footer-mail-link">info@stmforum.com</a>
                                </p>
                            </li>
                            <li>
                                <a href="https://www.facebook.com/stmforum" target="_blank"><i
                                        class="fa fa-facebook"></i></a>
                                <a href="https://twitter.com/stmforum" target="_blank"><i class="fa fa-twitter"></i></a>
                                <a href="https://www.instagram.com/stmforum/" target="_blank"><i
                                        class="fa fa-instagram"></i></a>
                                <a href="https://www.youtube.com/channel/UCiIGcnuSoTBqiYYs0j4hEpg" target="_blank"><i
                                        class="fa fa-youtube-play"></i></a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </footer>
    </footer>
</div>


<script src="js/lib-16d4bd2210.js"></script>

<script src="js/app-1fb94969ae.js"></script>


<!-- Facebook Pixel Code -->
<script data-cfasync="false">
    !function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                                                                n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
        n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
        document,'script','//connect.facebook.net/en_US/fbevents.js');

    fbq('init', '1675636279326416');
    fbq('track', "PageView");

</script>
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=1675636279326416&ev=PageView&noscript=1"/></noscript>
<!-- End Facebook Pixel Code -->

<!-- Mixpanel JS -->
<script type="text/javascript" data-cfasync="false">(function(e,b){if(!b.__SV){var a,f,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable time_event track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
    for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=e.createElement("script");a.type="text/javascript";a.async=!0;a.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"file:"===e.location.protocol&&"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js".match(/^\/\//)?"https://cdn.mxpnl.com/libs/mixpanel-2-latest.min.js":"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";f=e.getElementsByTagName("script")[0];f.parentNode.insertBefore(a,f)}})(document,window.mixpanel||[]);
mixpanel.init("3508e634d8c8dd65b3bb008c0e26fe01");
function source(){
    var source = null;

    if (document.referrer.search('https?://(.*)google.([^/?]*)') === 0) {
        source = 'Google';
    } else if (document.referrer.search('https?://(.*)bing.([^/?]*)') === 0) {
        source = 'Bing';
    } else if (document.referrer.search('https?://(.*)yahoo.([^/?]*)') === 0) {
        source = 'Yahoo';
    } else if (document.referrer.search('https?://(.*)facebook.([^/?]*)') === 0) {
        source = 'Facebook';
    } else if (document.referrer.search('https?://(.*)twitter.([^/?]*)') === 0) {
        source = 'Twitter';
    } else if (document.referrer.search('https?://(.*)charlesngo.([^/?]*)') === 0) {
        source = 'Charles Ngo';
    } else if (document.referrer.search('https?://(.*)mrgreen.([^/?]*)') === 0) {
        source = 'MrGreen Blog';
    } else if (document.referrer.search('https?://(.*)iamattila.([^/?]*)') === 0) {
        source = 'IamAttila';
    } else if (document.referrer.search('https?://(.*)click202.([^/?]*)') === 0) {
        source = 'Click202';
    } else if (document.referrer.search('https?://(.*)finchsells.([^/?]*)') === 0) {
        source = 'Finch';
    } else if (document.referrer.search('https?://(.*)fbqueen.([^/?]*)') === 0) {
        source = 'FBQueen';
    } else if (document.referrer.search('https?://(.*)viperchill.([^/?]*)') === 0) {
        source = 'ViperChill';
    } else {
        source = 'Other';
    }
    return source;
}

var last_visited_home_page = "Version 1";

mixpanel.register_once({"Source": source()});
mixpanel.register({"Last-visited Home Page": last_visited_home_page});

document.cookie = "source="+source()+"; path=/";
document.cookie = "last_visited_home_page="+last_visited_home_page+"; path=/";

mixpanel.track('Page View', {
    "Page Type": "Homepage",
    "Homepage Version": "Version 1 (video)",
    "Page Long Name": "Homepage - V1 (video)",
    "Visited Homepage": true
});
mixpanel.track('Homepage View', {
    "Page Type": "Homepage",
    "Homepage Version": "Version 1 (video)",
    "Page Long Name": "Homepage - V1 (video)",
    "Visited Homepage": true
});
</script>
<!-- end Mixpanel -->

<!-- This site is converting visitors into subscribers and customers with OptinMonster - http://optinmonster.com --><div id="om-rpchvbbqeg5vf3ce-holder"></div><script>var rpchvbbqeg5vf3ce,rpchvbbqeg5vf3ce_poll=function(){var r=0;return function(n,l){clearInterval(r),r=setInterval(n,l)}}();!function(e,t,n){if(e.getElementById(n)){rpchvbbqeg5vf3ce_poll(function(){if(window['om_loaded']){if(!rpchvbbqeg5vf3ce){rpchvbbqeg5vf3ce=new OptinMonsterApp();return rpchvbbqeg5vf3ce.init({u:"17704.323938",staging:0,dev:0,beta:0});}}},25);return;}var d=false,o=e.createElement(t);o.id=n,o.src="//a.optnmnstr.com/app/js/api.min.js",o.onload=o.onreadystatechange=function(){if(!d){if(!this.readyState||this.readyState==="loaded"||this.readyState==="complete"){try{d=om_loaded=true;rpchvbbqeg5vf3ce=new OptinMonsterApp();rpchvbbqeg5vf3ce.init({u:"17704.323938",staging:0,dev:0,beta:0});o.onload=o.onreadystatechange=null;}catch(t){}}}};(document.getElementsByTagName("head")[0]||document.documentElement).appendChild(o)}(document,"script","omapi-script");</script><!-- / OptinMonster -->

<!-- Drip -->
<script type="text/javascript">
    var _dcq = _dcq || [];
    var _dcs = _dcs || {};
    _dcs.account = '2842739';

    (function() {
        var dc = document.createElement('script');
        dc.type = 'text/javascript'; dc.async = true;
        dc.src = '//tag.getdrip.com/2842739.js';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(dc, s);
    })();
</script>

<!-- begin GA Ecommerce code -->
<script>
ga('require', 'ec');

ga('ec:addImpression', {
  'id': 'STM1',
  'name': 'STM Forum',
  'variant': 'V1'                //STM Forum version, leave V1 for now
});

ga('send', {
  hitType: 'pageview',
  title: 'STM Homepage',
  eventCategory: 'Homepage',
  eventLabel: 'V1'       //STM Forum version, leave V1 for now
});
</script>
<!-- end GA Ecommerce code -->

</body>
</html>
