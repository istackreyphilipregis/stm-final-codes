Custom classes/libraries made for AMember and VBulletin for STM.

## Instuctions - AMember Changes folder:

1. Upload all the contents of the controllers folder in /public_html/amember/application/default/controllers folder in the server.

2. Upload all the contents of the views folder in /public_html/amember/application/default/views folder in the server.

3. Upload all the contents of the models folder in /public_html/amember/application/default/models folder in the server.

4. Upload all the contents of the library folder in /public_html/amember/library/Am/Form folder in the server.

## Instruction - STM folder:

1. Copy all the contents of the forum folder under the segment_events folder and upload them to /public_html/forum folder of the server.

2. Copy the contents of the includes folder and then upload them to /public_html/forum/includes folder of the server.

`Note: Disregard the home, join and templates_styles folder, the only folder that is useful here is the forum folder inside the segment_events folder.`

## Instruction - stm-libraries:
1. Edit the config.json file in the stm-libraries folder before uploading all the contents on the server.

2. Just upload this folder "stm-libraries" in the document root or the /public_html folder of the server.
