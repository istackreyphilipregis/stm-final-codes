<?php

class InvoicePaymentCustom extends Am_Table_WithData {
    protected $_key = 'invoice_payment_id';
    protected $_table = '?_invoice_payment';
    protected $_recordClass = 'InvoicePayment';

    function getInvoicePaymentByUser($userId)
    {
        return $this->_db->selectCell("SELECT SUM(amount) FROM ?_invoice_payment WHERE user_id=?d", $userId);
    }
}
